package com.zenika.poei.amismaisonduvin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmisMaisonDuVinApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmisMaisonDuVinApplication.class, args);
    }

}
