package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.repository.JdbcMemberRepository;
import com.zenika.poei.amismaisonduvin.repository.MemberRepository;
import com.zenika.poei.amismaisonduvin.repository.ReviewRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MemberController {
    private MemberRepository memberRepository;
    private ReviewRepository reviewRepository;

    public MemberController(MemberRepository memberRepository, ReviewRepository reviewRepository){
        this.memberRepository = memberRepository;
        this.reviewRepository = reviewRepository;
    }

    @GetMapping("/members")
    public List<Member> getMembers(){
        return this.memberRepository.getMembers();
    }

    @GetMapping("/members/{id}")
    public Member getMemberById(@PathVariable int id){
        return this.memberRepository.getMemberById(id);
    }

    @PostMapping("/members")
    public void addMember(@RequestBody Member newMember){
        memberRepository.addMember(newMember);

    }

    @PutMapping("/members/{id}")
    public void updateMember(@PathVariable int id, @RequestBody Member newMember){
        newMember.setId(id);
        this.memberRepository.updateMember(id, newMember);
    }

    @DeleteMapping("/members/{id}")
    public void deleteMember(@PathVariable int id){
        this.reviewRepository.deleteReviewsByMember(id);
        this.memberRepository.deleteMember(id);
    }

}
