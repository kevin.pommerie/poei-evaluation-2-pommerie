package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.repository.ReviewRepository;
import com.zenika.poei.amismaisonduvin.repository.WineRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReviewController {
    private ReviewRepository reviewRepository;

    public ReviewController(ReviewRepository reviewRepository){
        this.reviewRepository = reviewRepository;
    }


    @PostMapping("/wines/{id}/reviews")
    public void addReview(@RequestBody Review review){
        this.reviewRepository.addReview(review);
    }

    @GetMapping("/wines/reviews")
    public List<Review> getReviews(){
        return this.reviewRepository.getReviews();
    }

    @GetMapping("/wines/{id}/reviews")
    public List<Review> getReviewsByWine(@PathVariable int id){
        return this.reviewRepository.getReviewsByWine(id);
    }

    @DeleteMapping("/wines/{id}/reviews")
    public void deleteReviews(@PathVariable int id){
        this.reviewRepository.deleteReviewsByWine(id);
    }
}
