package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.repository.ReviewRepository;
import com.zenika.poei.amismaisonduvin.repository.WineRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WineController {
    private WineRepository wineRepository;
    private ReviewRepository reviewRepository;

    public WineController(WineRepository wineRepository, ReviewRepository reviewRepository){
        this.wineRepository = wineRepository;
        this.reviewRepository = reviewRepository;
    }

    @GetMapping("/wines")
    public List<Wine> getWines(){
        return this.wineRepository.getWines();
    }

    @GetMapping("/wines?region={region}")
    public List<Wine> getWinesByRegion(@PathVariable String region){
        return this.wineRepository.getWinesByRegion(region);
    }

    @GetMapping("/wines/{id}")
    public Wine getWineById(@PathVariable int id){
        return this.wineRepository.getWineById(id);
    }

    @PostMapping("/wines")
    public void addWine(@RequestBody Wine newWine){
        this.wineRepository.addWine(newWine);
    }

    @PutMapping("/wines/{id}")
    public void updateWine(@PathVariable int id, @RequestBody Wine newWine){
        Wine oldWine = wineRepository.getWineById(id);
        this.wineRepository.updateWine(oldWine, newWine);
    }

    @DeleteMapping("/wines/{id}")
    public void deleteWine(@PathVariable int id){
        this.reviewRepository.deleteReviewsByWine(id);
        this.wineRepository.deleteWine(id);
    }
}
