package com.zenika.poei.amismaisonduvin.domain;

import com.zenika.poei.amismaisonduvin.domain.exception.InvalidMemberException;
import org.springframework.util.StringUtils;

public class Member {

    private Integer id;
    private String pseudonym;
    private String firstName;
    private String lastName;

    public Member(Integer id, String pseudonym, String firstName, String lastName){
        this.id = id;
        this.pseudonym = pseudonym;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Member(){

    }

    public Integer getId() {
        return id;
    }

    public Member setId(Integer id) {
        this.id = id;

        return this;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public Member setPseudonym(String pseudonym) {
        if (!StringUtils.hasText(pseudonym)) {
            throw new InvalidMemberException("Pseudonym is empty");
        }

        this.pseudonym = pseudonym;

        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Member setFirstName(String firstName) {
        if (!StringUtils.hasText(firstName)) {
            throw new InvalidMemberException("FirstName is empty");
        }

        this.firstName = firstName;

        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Member setLastName(String lastName) {
        if (!StringUtils.hasText(lastName)) {
            throw new InvalidMemberException("LastName is empty");
        }

        this.lastName = lastName;

        return this;
    }
}
