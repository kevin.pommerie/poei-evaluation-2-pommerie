package com.zenika.poei.amismaisonduvin.domain;

import com.zenika.poei.amismaisonduvin.domain.exception.InvalidReviewException;

public class Review {

    private int id_member;
    private int id_wine;
    private int score;
    private String comment;

    public Review(){
        id_member = 0;
        id_wine = 0;
        score = 0;
        comment ="totalement nul";
    }

    public Review(Member member, Wine wine, int score, String comment) {
        id_member = member.getId();
        id_wine = wine.getId();
        setScore(score);
        setComment(comment);
    }

    public Review(int id_member, int id_wine, int score, String comment) {
        this.id_member = id_member;
        this.id_wine = id_wine;
        this.score = score;
        this.comment = comment;
    }

    public int getIdMember() {
        return id_member;
    }

    public void setMember(Member member) {
        if (member == null) {
            throw new InvalidReviewException("Member is undefined");
        }
    }

    public int getScore() {
        return score;
    }

    public Review setScore(int score) {
        if (score <0 || score > 5) {
            throw new InvalidReviewException("Score must be between 0 and 5");
        }

        this.score = score;

        return this;
    }

    public String getComment() {
        return comment;
    }

    public Review setComment(String comment) {
        this.comment = comment;

        return this;
    }

    public int getIdWine() {
        return id_wine;
    }
}
