package com.zenika.poei.amismaisonduvin.domain.exception;

public class InvalidWineException extends FunctionalException {

    public InvalidWineException() {
    }

    public InvalidWineException(String message) {
        super(message);
    }

    public InvalidWineException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidWineException(Throwable cause) {
        super(cause);
    }
}
