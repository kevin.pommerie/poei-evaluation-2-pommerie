package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Member;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
@Primary
@Repository
public class JdbcMemberRepository implements MemberRepository {
    private final JdbcTemplate jdbcTemplate;
    private List<Member> members;

    public JdbcMemberRepository(List<Member> members, JdbcTemplate jdbcTemplate) {
        this.members = members;
        this.jdbcTemplate = jdbcTemplate;
    }

    public static final RowMapper<Member> ROW_MAPPER_MEMBER = new RowMapper<Member>() {
        @Override
        public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Member(rs.getInt("id"),
                    rs.getString("pseudonym"),
                    rs.getString("first_name"),
                    rs.getString("last_name"));
        }
    };


    @Override
    public List<Member> getMembers() {
        return jdbcTemplate.query(
                "select * from members", ROW_MAPPER_MEMBER);
    }

    @Override
    public Member getMemberById(int id) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM members WHERE id = ?", ROW_MAPPER_MEMBER, id);
    }

    @Override
    public void addMember(Member newMember) {

        //si un utilisateur possède déjà le même pseudonyme
        for (Member member : members) {
            if (newMember.getPseudonym() == member.getPseudonym()) {
                System.out.println("erreur : pseudonyme déjà utilisé par un autre membre");

            }
        }

        jdbcTemplate.queryForObject(" INSERT INTO members(first_name, last_name, pseudonym) " +
                        " VALUES (?,?,?) returning *",
                ROW_MAPPER_MEMBER,
                newMember.getFirstName(),
                newMember.getLastName(),
                newMember.getPseudonym());
    }

    @Override
    public void updateMember(int id, Member newMember) {

        //si un autre utilisateur possède déjà le même pseudonyme
        for (Member member : members) {
            if (newMember.getPseudonym() == member.getPseudonym() && newMember.getId() != member.getId()) {
                System.out.println("erreur : pseudonyme déjà utilisé par un autre membre");
            }
        }

        jdbcTemplate.queryForObject(" UPDATE members " +
                        " SET first_name = ?, " +
                        " last_name = ?, " +
                        " pseudonym = ? " +
                        " WHERE id = ? returning *",
                ROW_MAPPER_MEMBER,
                newMember.getFirstName(),
                newMember.getLastName(),
                newMember.getPseudonym(),
                id);

    }

    @Override
    public void deleteMember(int id) {
        jdbcTemplate.queryForObject("delete from members where id=?",
                ROW_MAPPER_MEMBER, id);
    }

    //TODO réparer delete member
}
