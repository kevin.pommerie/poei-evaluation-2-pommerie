package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
@Primary
@Repository
public class JdbcReviewRepository implements ReviewRepository {

    private final JdbcTemplate jdbcTemplate;

    JdbcReviewRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public static final RowMapper<Review> ROW_MAPPER_REVIEW = new RowMapper<Review>() {
        @Override
        public Review mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Review(rs.getInt("id_member"),
                    rs.getInt("id_wine"),
                    rs.getInt("score"),
                    rs.getString("comment"));
        }
    };

    @Override
    public void addReview(Review review) {
        jdbcTemplate.queryForObject(" insert into reviews(id_member, id_wine, score, comment) "
                        + " VALUES (?,?,?,?) returning *",
                ROW_MAPPER_REVIEW,
                review.getIdMember(),
                review.getIdWine(),
                review.getScore(),
                review.getComment());
    }

    @Override
    public List<Review> getReviews() {
        return jdbcTemplate.query(
                "select * from reviews", ROW_MAPPER_REVIEW);
    }

    @Override
    public List<Review> getReviewsByWine(int id) {
        return jdbcTemplate.query("SELECT * FROM reviews WHERE id_wine = ?",
                ROW_MAPPER_REVIEW, id);
    }

    @Override
    public void deleteReviewsByWine(int id) {
        jdbcTemplate.query("DELETE FROM reviews WHERE id_wine = ?",
                ROW_MAPPER_REVIEW, id);
    }

    @Override
    public void deleteReviewsByMember(int id) {
        jdbcTemplate.query("DELETE FROM reviews WHERE id_member = ?",
                ROW_MAPPER_REVIEW, id);
    }

    //TODO réparer add et delete review
}
