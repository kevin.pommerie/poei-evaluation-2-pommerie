package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
@Primary
@Repository
public class JdbcWineRepository implements WineRepository {
    private final JdbcTemplate jdbcTemplate;

    JdbcWineRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public static final RowMapper<Wine> ROW_MAPPER_WINE = new RowMapper<Wine>() {
        @Override
        public Wine mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Wine(rs.getInt("id"),
                    rs.getString("appellation"),
                    rs.getString("colour"),
                    rs.getString("region"),
                    rs.getString("variety"),
                    rs.getInt("vintage"));
        }
    };

    @Override
    public List<Wine> getWines() {
        return jdbcTemplate.query(
                "select * from wines", ROW_MAPPER_WINE);
    }

    @Override
    public List<Wine> getWinesByRegion(String region) {
        return jdbcTemplate.query("SELECT * FROM wines WHERE region = ?",
                ROW_MAPPER_WINE, region);
    }

    @Override
    public Wine getWineById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM wines WHERE id = ?",
                ROW_MAPPER_WINE, id);
    }

    @Override
    public void addWine(Wine newWine) {
        jdbcTemplate.queryForObject("insert into wines(appellation, colour, region, variety, vintage) "
                        + " VALUES (?,?,?,?,?) returning *",
                ROW_MAPPER_WINE,
                newWine.getAppellation(),
                newWine.getColour(),
                newWine.getRegion(),
                newWine.getVariety(),
                newWine.getVintage());
    }

    @Override
    public void updateWine(Wine oldWine, Wine newWine) {
        jdbcTemplate.queryForObject("UPDATE wines " +
                        " SET appellation = ?, colour = ?, region = ?, variety = ?, vintage = ? "
                        + " WHERE id = ? returning *",
                ROW_MAPPER_WINE,
                newWine.getAppellation(),
                newWine.getColour(),
                newWine.getRegion(),
                newWine.getVariety(),
                newWine.getVintage(),
                oldWine.getId());
    }

    @Override
    public void deleteWine(int id) {
        jdbcTemplate.queryForObject("delete from wines where id=?",
                ROW_MAPPER_WINE, id);
    }

    //TODO réparer delete wine
}
