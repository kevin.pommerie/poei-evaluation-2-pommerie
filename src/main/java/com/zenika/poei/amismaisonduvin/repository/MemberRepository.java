package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Member;

import java.util.List;

public interface MemberRepository {

    List<Member> getMembers();

    Member getMemberById(int id);

    void addMember(Member newMember);

    void updateMember(int id, Member newMember);

    void deleteMember(int id);
}
