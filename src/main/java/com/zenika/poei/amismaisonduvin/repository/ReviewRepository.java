package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Review;

import java.util.List;

public interface ReviewRepository {
    void addReview(Review review);

    List<Review> getReviews();

    List<Review> getReviewsByWine(int id);

    void deleteReviewsByWine(int id);

    void deleteReviewsByMember(int id);
}
