package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Wine;

import java.util.List;

public interface WineRepository {
    List<Wine> getWines();

    List<Wine> getWinesByRegion(String region);
    
    Wine getWineById(int id);

    void addWine(Wine newWine);

    void updateWine(Wine oldWine, Wine newWine);

    void deleteWine(int id);
}
