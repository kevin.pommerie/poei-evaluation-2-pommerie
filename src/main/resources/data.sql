INSERT INTO members(first_name, last_name, pseudonym)
VALUES ('first_name_1', 'last_name_1', 'pseudonym_1'),
       ('first_name_2', 'last_name_2', 'pseudonym_2');

INSERT INTO wines(appellation, colour, region, variety, vintage)
VALUES ('my_appellation', 'my_colour', 'my_region', 'my_variety', 1991);

INSERT INTO reviews(id_member, id_wine, score, comment)
VALUES (1, 1, 5, 'je l aime d amour');