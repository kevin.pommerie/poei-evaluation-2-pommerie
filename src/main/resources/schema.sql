drop table reviews;
drop table members;
drop table wines;



create table if not exists members
(
    id integer generated always as identity primary key,
    first_name text not null,
    last_name text not null,
    pseudonym text unique not null
);

create table if not exists wines
(
    id integer generated always as identity primary key,
    appellation text not null,
    colour text not null,
    region text not null,
    variety text not null,
    vintage integer not null
);

create table if not exists reviews
(
    id_member integer not null references members (id),
    id_wine integer not null references wines (id),
    score integer not null,
    comment text not null
);


